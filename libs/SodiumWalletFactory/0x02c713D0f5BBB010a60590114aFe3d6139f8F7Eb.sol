// Sources flattened with hardhat v2.12.4 https://hardhat.org

// File contracts/interfaces/ISodiumWalletFactory.sol

// SPDX-License-Identifier: MIT
pragma solidity ^0.8.16;

interface ISodiumWalletFactory {
    event WalletCreated(address indexed owner, address wallet);

    function createWallet(address borrower) external returns (address);
}

// File contracts/interfaces/ISodiumWallet.sol

/// @notice Smart wallet to hold loan collateral on the Sodium Protocol
/// @dev This contract is deployed as the implementation for proxy wallets
interface ISodiumWallet {
    /// @param borrower_ The owner of this wallet
    /// @param core_ The address of the Core
    /// @param registry_ Used by the wallets to determine external call permission
    function initialize(address borrower_, address core_, address registry_) external;

    /// @notice Used by borrower to make calls with their Sodium wallet
    /// @dev Uses `registry` to determine call (address & function selector) permission
    /// @param contractAddresses_ An in-order array of the addresses to which the calls are to be made
    /// @param calldatas_ The in-order calldatas to be used during those calls (elements at same index correspond)
    /// @param values_ The in-order Wei amounts to be sent with those calls (again elements at same index correspond)
    function execute(
        address[] calldata contractAddresses_,
        bytes[] memory calldatas_,
        uint256[] calldata values_
    ) external payable;

    /// @notice Called by core to transfer an ERC721 token held in wallet
    function transferERC721(address recipient_, address tokenAddress_, uint256 tokenId_) external;

    /// @notice Called by core to transfer an ERC1155 token held in wallet
    function transferERC1155(address recipient, address tokenAddress, uint256 tokenId) external;
}

// File @openzeppelin/contracts/proxy/Clones.sol@v4.8.0

/**
 * @dev https://eips.ethereum.org/EIPS/eip-1167[EIP 1167] is a standard for
 * deploying minimal proxy contracts, also known as "clones".
 *
 * > To simply and cheaply clone contract functionality in an immutable way, this standard specifies
 * > a minimal bytecode implementation that delegates all calls to a known, fixed address.
 *
 * The library includes functions to deploy a proxy using either `create` (traditional deployment) or `create2`
 * (salted deterministic deployment). It also includes functions to predict the addresses of clones deployed using the
 * deterministic method.
 *
 * _Available since v3.4._
 */
library Clones {
    /**
     * @dev Deploys and returns the address of a clone that mimics the behaviour of `implementation`.
     *
     * This function uses the create opcode, which should never revert.
     */
    function clone(address implementation) internal returns (address instance) {
        /// @solidity memory-safe-assembly
        assembly {
            // Cleans the upper 96 bits of the `implementation` word, then packs the first 3 bytes
            // of the `implementation` address with the bytecode before the address.
            mstore(0x00, or(shr(0xe8, shl(0x60, implementation)), 0x3d602d80600a3d3981f3363d3d373d3d3d363d73000000))
            // Packs the remaining 17 bytes of `implementation` with the bytecode after the address.
            mstore(0x20, or(shl(0x78, implementation), 0x5af43d82803e903d91602b57fd5bf3))
            instance := create(0, 0x09, 0x37)
        }
        require(instance != address(0), "ERC1167: create failed");
    }

    /**
     * @dev Deploys and returns the address of a clone that mimics the behaviour of `implementation`.
     *
     * This function uses the create2 opcode and a `salt` to deterministically deploy
     * the clone. Using the same `implementation` and `salt` multiple time will revert, since
     * the clones cannot be deployed twice at the same address.
     */
    function cloneDeterministic(address implementation, bytes32 salt) internal returns (address instance) {
        /// @solidity memory-safe-assembly
        assembly {
            // Cleans the upper 96 bits of the `implementation` word, then packs the first 3 bytes
            // of the `implementation` address with the bytecode before the address.
            mstore(0x00, or(shr(0xe8, shl(0x60, implementation)), 0x3d602d80600a3d3981f3363d3d373d3d3d363d73000000))
            // Packs the remaining 17 bytes of `implementation` with the bytecode after the address.
            mstore(0x20, or(shl(0x78, implementation), 0x5af43d82803e903d91602b57fd5bf3))
            instance := create2(0, 0x09, 0x37, salt)
        }
        require(instance != address(0), "ERC1167: create2 failed");
    }

    /**
     * @dev Computes the address of a clone deployed using {Clones-cloneDeterministic}.
     */
    function predictDeterministicAddress(
        address implementation,
        bytes32 salt,
        address deployer
    ) internal pure returns (address predicted) {
        /// @solidity memory-safe-assembly
        assembly {
            let ptr := mload(0x40)
            mstore(add(ptr, 0x38), deployer)
            mstore(add(ptr, 0x24), 0x5af43d82803e903d91602b57fd5bf3ff)
            mstore(add(ptr, 0x14), implementation)
            mstore(ptr, 0x3d602d80600a3d3981f3363d3d373d3d3d363d73)
            mstore(add(ptr, 0x58), salt)
            mstore(add(ptr, 0x78), keccak256(add(ptr, 0x0c), 0x37))
            predicted := keccak256(add(ptr, 0x43), 0x55)
        }
    }

    /**
     * @dev Computes the address of a clone deployed using {Clones-cloneDeterministic}.
     */
    function predictDeterministicAddress(
        address implementation,
        bytes32 salt
    ) internal view returns (address predicted) {
        return predictDeterministicAddress(implementation, salt, address(this));
    }
}

// File contracts/SodiumWalletFactory.sol

/// @notice Simple clone factory for creating minimal proxy Sodium Wallets
contract SodiumWalletFactory is ISodiumWalletFactory {
    address public implementation;
    address public registry;

    /// @param implementation_ The contract to which wallets deployed by this contract delegate their calls
    /// @param registry_ Used by the wallets to determine external call permission
    constructor(address implementation_, address registry_) {
        implementation = implementation_;
        registry = registry_;
    }

    /// @notice Called by the Core to create new wallets
    /// @dev Deploys a minimal EIP-1167 proxy that delegates its calls to `implementation`
    /// @param requester The owner of the new wallet
    function createWallet(address requester) external override returns (address) {
        address wallet = Clones.clone(implementation);
        ISodiumWallet(wallet).initialize(requester, msg.sender, registry);
        emit WalletCreated(requester, wallet);

        return wallet;
    }
}