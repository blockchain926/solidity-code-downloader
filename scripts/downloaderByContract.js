var path = require('path');
var fs = require('fs');
var fetch = require('node-fetch');
var downloadedContracts = require('./downloadedContracts');

require('dotenv').config();

const projectPath = process.cwd();
const downloadedContractsPath = path.resolve(projectPath, 'scripts/downloadedContracts.json');
const codeLibsPath = path.resolve(projectPath, 'libs');

const downloader = async (contractAddr) => {
    if (downloadedContracts.includes(contractAddr)) {
        return;
    }
    console.log('download: ', 'contractAddr:', contractAddr);
    const resp = await fetch(`${process.env.API_URL}?module=contract&action=getsourcecode&address=${contractAddr}&apikey=${process.env.API_KEY}`).catch(e => {
        console.error("request api error", e)
        return undefined;
    })
    if (!resp) {
        return;
    }
    const r = await resp.json()
    if (r.message === 'OK' && r.result[0]?.SourceCode?.length > 0) {
        const code = r.result[0]

        let codeStr;
        let sourceCode = code.SourceCode.trimLeft().trimRight();

        if (sourceCode.startsWith("{{") && sourceCode.endsWith("}}")) {
            sourceCode = sourceCode.substring(1, sourceCode.length - 1)
            const obj = JSON.parse(sourceCode);
            if (obj?.sources) {
                let code = "";
                const files = obj?.sources;
                for (let key in files) {
                    code = `${code}\n// File: ${key}\n${files[key]["content"]}\n`;
                }
                codeStr = code;
            }
        } else {
            codeStr = sourceCode;
        }
        downloadedContracts.push(contractAddr);
        fs.writeFileSync(downloadedContractsPath, JSON.stringify(downloadedContracts));
        console.log('downloaded: ', 'contractName:', code.ContractName);
        const _projectPath = path.resolve(codeLibsPath, code.ContractName);
        if (!fs.existsSync(_projectPath)) {
            fs.mkdirSync(_projectPath)
        }

        const solPath = path.resolve(codeLibsPath, `${code.ContractName}/${contractAddr}.sol`);
        fs.writeFileSync(solPath, codeStr);
    }
}

downloader(process.argv[2])