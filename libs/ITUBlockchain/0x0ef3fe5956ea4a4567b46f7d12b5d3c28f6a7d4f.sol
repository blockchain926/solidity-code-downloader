// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IERC165 {
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}

abstract contract ERC165 is IERC165 {
    function supportsInterface(
        bytes4 interfaceId
    ) public view virtual override returns (bool) {
        return interfaceId == type(IERC165).interfaceId;
    }
}

interface IERC721 is IERC165 {
    event Transfer(
        address indexed from,
        address indexed to,
        uint256 indexed tokenId
    );

    function balanceOf(address owner) external view returns (uint256 balance);

    function ownerOf(uint256 tokenId) external view returns (address owner);
}

interface IERC721Metadata is IERC721 {
    function name() external view returns (string memory);

    function symbol() external view returns (string memory);

    function tokenURI(uint256 tokenId) external view returns (string memory);
}

contract ERC721 is ERC165, IERC721, IERC721Metadata {
    mapping(uint256 => address) private _owners;

    mapping(address => uint256) private _balances;

    function supportsInterface(
        bytes4 interfaceId
    ) public view virtual override(ERC165, IERC165) returns (bool) {
        return
            interfaceId == type(IERC721).interfaceId ||
            interfaceId == type(IERC721Metadata).interfaceId ||
            super.supportsInterface(interfaceId);
    }

    function balanceOf(
        address owner
    ) public view virtual override returns (uint256) {
        require(owner != address(0));
        return _balances[owner];
    }

    function ownerOf(
        uint256 tokenId
    ) public view virtual override returns (address) {
        address owner = _ownerOf(tokenId);
        require(owner != address(0));
        return owner;
    }

    function name() public view virtual override returns (string memory) {
        return "ITUBlockchain";
    }

    function symbol() public view virtual override returns (string memory) {
        return "ITUBC";
    }

    function tokenURI(
        uint256
    ) public view virtual override returns (string memory) {
        string memory baseURI = _baseURI();
        return baseURI;
    }

    function _baseURI() internal view virtual returns (string memory) {
        return "";
    }

    function _ownerOf(uint256 tokenId) internal view virtual returns (address) {
        return _owners[tokenId];
    }

    function _exists(uint256 tokenId) internal view virtual returns (bool) {
        return _ownerOf(tokenId) != address(0);
    }

    function _mint(address to) internal virtual {
        unchecked {
            _balances[to] += 1;
        }

        _owners[1773] = to;

        emit Transfer(address(0), to, 1773);
    }
}

/**
 * @author ITU Blockchain - https://twitter.com/ITUblockchain - https://itublockchain.com/
 * @notice Modified ERC-721 contract to use in Twitter PFP
 * @dev Does not keep the functionality of the NFT standart
 * @dev Use at your own risk
 */
contract ITUBlockchain is ERC721 {
    string private uri =
        "ipfs://QmcNMfSL91XkiFzQEmUHrRudg8KTjw2Vpr6qG1La3JXPUW";

    constructor() {
        _mint(msg.sender);
    }

    function tokenURI(uint256) public view override returns (string memory) {
        string memory baseUri = _baseURI();
        return baseUri;
    }

    function setURI(string memory newURI) external {
        require(ownerOf(1773) == msg.sender);
        uri = newURI;
    }

    function _baseURI() internal view override returns (string memory) {
        return uri;
    }
}
