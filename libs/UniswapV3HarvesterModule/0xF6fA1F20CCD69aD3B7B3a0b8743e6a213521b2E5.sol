
// File: UniswapV3HarvesterModule.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "AutomationCompatible.sol";
import "Pausable.sol";

import "INonfungiblePositionManager.sol";

import {BaseModule} from "BaseModule.sol";
import {UniswapV3HarvesterModuleConstants} from "UniswapV3HarvesterModuleConstants.sol";

/// @title UniswapV3HarvesterModule
/// @notice Allows the module the processing of fees accumulated in each NFT position owned by the
///         safe (vault_msig) under a specific cadence.
contract UniswapV3HarvesterModule is BaseModule, AutomationCompatible, Pausable, UniswapV3HarvesterModuleConstants {
    ////////////////////////////////////////////////////////////////////////////
    // STORAGE
    ////////////////////////////////////////////////////////////////////////////
    address public guardian;

    uint256 public lastProcessingTimestamp;
    uint256 public processingInterval;

    ////////////////////////////////////////////////////////////////////////////
    // ERRORS
    ////////////////////////////////////////////////////////////////////////////

    error NotGovernance(address caller);
    error NotGovernanceOrGuardian(address caller);
    error NotKeeper(address caller);

    error TooSoon(uint256 lastProcessing, uint256 timestamp);

    error NoFeesCollected(uint256 tokenId);
    error NotOwnedNft(uint256 tokenId);

    error ZeroIntervalPeriod();
    error ZeroAddress();
    error ModuleMisconfigured();

    ////////////////////////////////////////////////////////////////////////////
    // EVENTS
    ////////////////////////////////////////////////////////////////////////////

    event ProcessingIntervalUpdated(uint256 oldProcessingInterval, uint256 newProcessingInterval, uint256 timestamp);
    event GuardianUpdated(address indexed oldGuardian, address indexed newGuardian, uint256 timestamp);

    ////////////////////////////////////////////////////////////////////////////
    // MODIFIERS
    ////////////////////////////////////////////////////////////////////////////

    /// @notice Checks whether a call is from governance
    modifier onlyGovernance() {
        if (msg.sender != GOVERNANCE) revert NotGovernance(msg.sender);
        _;
    }

    /// @notice Checks whether a call is from governance or guardian
    modifier onlyGovernanceOrGuardian() {
        if (msg.sender != GOVERNANCE && msg.sender != guardian) revert NotGovernanceOrGuardian(msg.sender);
        _;
    }

    /// @notice Checks whether a call is from the keeper.
    modifier onlyKeeper() {
        if (msg.sender != CHAINLINK_KEEPER_REGISTRY) revert NotKeeper(msg.sender);
        _;
    }

    /// @param _processingInterval Frequency in seconds at which `feeToken` will be processed
    /// @param _guardian Address allowed to pause contract
    constructor(uint256 _processingInterval, address _guardian) {
        if (_processingInterval == 0) revert ZeroIntervalPeriod();
        if (_guardian == address(0)) revert ZeroAddress();
        processingInterval = _processingInterval;
        guardian = _guardian;
    }

    ////////////////////////////////////////////////////////////////////////////
    // PUBLIC: Governance
    ////////////////////////////////////////////////////////////////////////////

    /// @notice Updates the duration for the processing of the UniV3 fees. Can only be called by owner.
    /// @param _processingInterval The new frequency period in seconds for processing `feeToken` in storage.
    function setProcessingInterval(uint256 _processingInterval) external onlyGovernance {
        if (_processingInterval == 0) revert ZeroIntervalPeriod();

        uint256 oldProcessingInterval = processingInterval;

        processingInterval = _processingInterval;
        emit ProcessingIntervalUpdated(oldProcessingInterval, _processingInterval, block.timestamp);
    }

    /// @notice  Updates the guardian address. Only callable by governance.
    /// @param _guardian Address which will become guardian
    function setGuardian(address _guardian) external onlyGovernance {
        if (_guardian == address(0)) revert ZeroAddress();
        address oldGuardian = guardian;
        guardian = _guardian;
        emit GuardianUpdated(oldGuardian, _guardian, block.timestamp);
    }

    /// @dev Pauses the contract, which prevents executing performUpkeep.
    function pause() external onlyGovernanceOrGuardian {
        _pause();
    }

    /// @dev Unpauses the contract.
    function unpause() external onlyGovernance {
        _unpause();
    }

    ////////////////////////////////////////////////////////////////////////////
    // PUBLIC: Keeper
    ////////////////////////////////////////////////////////////////////////////

    /// @dev Contains the logic that should be executed on-chain when
    ///      `checkUpkeep` returns true.
    function performUpkeep(bytes calldata performData) external override whenNotPaused onlyKeeper {
        /// @dev safety check, ensuring onchain module is config
        if (!SAFE.isModuleEnabled(address(this))) revert ModuleMisconfigured();

        if ((block.timestamp - lastProcessingTimestamp) < processingInterval) {
            revert TooSoon(lastProcessingTimestamp, block.timestamp);
        }

        uint256[] memory nftIds = abi.decode(performData, (uint256[]));
        uint256 idsLength = nftIds.length;
        if (idsLength > 0) {
            for (uint256 i; i < idsLength;) {
                (uint256 amount0, uint256 amount1) = _collect(nftIds[i]);
                /// @dev protection for empty fees TokenIds pass by keeper
                if (amount0 == 0 && amount1 == 0) revert NoFeesCollected(nftIds[i]);
                unchecked {
                    ++i;
                }
            }
            lastProcessingTimestamp = block.timestamp;
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // PUBLIC VIEW
    ////////////////////////////////////////////////////////////////////////////

    /// @notice Checks whether an upkeep is to be performed.
    /// @return upkeepNeeded_ A boolean indicating whether an upkeep is to be performed.
    /// @return performData_ The calldata to be passed to the upkeep function.
    function checkUpkeep(bytes calldata)
        external
        override
        cannotExecute
        returns (bool upkeepNeeded_, bytes memory performData_)
    {
        if (!SAFE.isModuleEnabled(address(this)) && (block.timestamp - lastProcessingTimestamp) < processingInterval) {
            // NOTE: explicit early return to checking rest of logic if these conditions are not met
            return (upkeepNeeded_, performData_);
        }

        uint256[] memory nftIndexes = _getNftsIndexOwned();
        uint256 length = nftIndexes.length;

        // NOTE: helpers to store which nft ids have indeed fees accumulated
        uint256[] memory nftsWithFees = new uint256[](length);
        uint256 nftsWithFeesLength;

        for (uint256 i; i < length;) {
            (uint256 amount0, uint256 amount1) = _collect(nftIndexes[i]);
            if (amount0 > 0 || amount1 > 0) {
                nftsWithFees[nftsWithFeesLength] = nftIndexes[i];
                unchecked {
                    ++nftsWithFeesLength;
                }
            }
            unchecked {
                ++i;
            }
        }

        if (nftsWithFeesLength != length) {
            // NOTE: truncate length
            assembly {
                mstore(nftsWithFees, nftsWithFeesLength)
            }
        }

        if (nftsWithFeesLength > 0) {
            upkeepNeeded_ = true;
            performData_ = abi.encode(nftsWithFees);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // INTERNAL
    ////////////////////////////////////////////////////////////////////////////

    /// @return TokenIds owned by governance
    function _getNftsIndexOwned() internal view returns (uint256[] memory) {
        uint256 totalNftOwned = UNIV3_POSITION_MANAGER.balanceOf(GOVERNANCE);
        uint256[] memory tokenIds = new uint256[](totalNftOwned);
        for (uint256 i; i < totalNftOwned;) {
            tokenIds[i] = UNIV3_POSITION_MANAGER.tokenOfOwnerByIndex(GOVERNANCE, i);
            unchecked {
                ++i;
            }
        }
        return tokenIds;
    }

    /// @param tokenId TokenId to claim fees from
    /// @return amount0 Token fees accumulated of token0
    /// @return amount1 Token fees accumulated of token1
    function _collect(uint256 tokenId) internal returns (uint256 amount0, uint256 amount1) {
        if (UNIV3_POSITION_MANAGER.ownerOf(tokenId) != GOVERNANCE) revert NotOwnedNft(tokenId);

        INonfungiblePositionManager.CollectParams memory params =
            INonfungiblePositionManager.CollectParams(tokenId, GOVERNANCE, UINT128_MAX, UINT128_MAX);
        bytes memory data = _checkTransactionAndExecuteReturningData(
            SAFE, address(UNIV3_POSITION_MANAGER), abi.encodeCall(INonfungiblePositionManager.collect, params)
        );
        (amount0, amount1) = abi.decode(data, (uint256, uint256));
    }
}


// File: AutomationCompatible.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "AutomationBase.sol";
import "AutomationCompatibleInterface.sol";

abstract contract AutomationCompatible is AutomationBase, AutomationCompatibleInterface {}


// File: AutomationBase.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract AutomationBase {
  error OnlySimulatedBackend();

  /**
   * @notice method that allows it to be simulated via eth_call by checking that
   * the sender is the zero address.
   */
  function preventExecution() internal view {
    if (tx.origin != address(0)) {
      revert OnlySimulatedBackend();
    }
  }

  /**
   * @notice modifier that allows it to be simulated via eth_call by checking
   * that the sender is the zero address.
   */
  modifier cannotExecute() {
    preventExecution();
    _;
  }
}


// File: AutomationCompatibleInterface.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface AutomationCompatibleInterface {
  /**
   * @notice method that is simulated by the keepers to see if any work actually
   * needs to be performed. This method does does not actually need to be
   * executable, and since it is only ever simulated it can consume lots of gas.
   * @dev To ensure that it is never called, you may want to add the
   * cannotExecute modifier from KeeperBase to your implementation of this
   * method.
   * @param checkData specified in the upkeep registration so it is always the
   * same for a registered upkeep. This can easily be broken down into specific
   * arguments using `abi.decode`, so multiple upkeeps can be registered on the
   * same contract and easily differentiated by the contract.
   * @return upkeepNeeded boolean to indicate whether the keeper should call
   * performUpkeep or not.
   * @return performData bytes that the keeper should call performUpkeep with, if
   * upkeep is needed. If you would like to encode data to decode later, try
   * `abi.encode`.
   */
  function checkUpkeep(bytes calldata checkData) external returns (bool upkeepNeeded, bytes memory performData);

  /**
   * @notice method that is actually executed by the keepers, via the registry.
   * The data returned by the checkUpkeep simulation will be passed into
   * this method to actually be executed.
   * @dev The input to this method should not be trusted, and the caller of the
   * method should not even be restricted to any single registry. Anyone should
   * be able call it, and the input should be validated, there is no guarantee
   * that the data passed in is the performData returned from checkUpkeep. This
   * could happen due to malicious keepers, racing keepers, or simply a state
   * change while the performUpkeep transaction is waiting for confirmation.
   * Always validate the data passed in.
   * @param performData is the data which was passed back from the checkData
   * simulation. If it is encoded, it can easily be decoded into other types by
   * calling `abi.decode`. This data should not be trusted, and should be
   * validated against the contract's current state.
   */
  function performUpkeep(bytes calldata performData) external;
}


// File: Pausable.sol
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (security/Pausable.sol)

pragma solidity ^0.8.0;

import "Context.sol";

/**
 * @dev Contract module which allows children to implement an emergency stop
 * mechanism that can be triggered by an authorized account.
 *
 * This module is used through inheritance. It will make available the
 * modifiers `whenNotPaused` and `whenPaused`, which can be applied to
 * the functions of your contract. Note that they will not be pausable by
 * simply including this module, only once the modifiers are put in place.
 */
abstract contract Pausable is Context {
    /**
     * @dev Emitted when the pause is triggered by `account`.
     */
    event Paused(address account);

    /**
     * @dev Emitted when the pause is lifted by `account`.
     */
    event Unpaused(address account);

    bool private _paused;

    /**
     * @dev Initializes the contract in unpaused state.
     */
    constructor() {
        _paused = false;
    }

    /**
     * @dev Returns true if the contract is paused, and false otherwise.
     */
    function paused() public view virtual returns (bool) {
        return _paused;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is not paused.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    modifier whenNotPaused() {
        require(!paused(), "Pausable: paused");
        _;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is paused.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    modifier whenPaused() {
        require(paused(), "Pausable: not paused");
        _;
    }

    /**
     * @dev Triggers stopped state.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    function _pause() internal virtual whenNotPaused {
        _paused = true;
        emit Paused(_msgSender());
    }

    /**
     * @dev Returns to normal state.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    function _unpause() internal virtual whenPaused {
        _paused = false;
        emit Unpaused(_msgSender());
    }
}


// File: Context.sol
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}


// File: INonfungiblePositionManager.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

interface INonfungiblePositionManager {
    struct CollectParams {
        uint256 tokenId;
        address recipient;
        uint128 amount0Max;
        uint128 amount1Max;
    }

    function balanceOf(address owner) external view returns (uint256);
    
    function collect(CollectParams memory params)
        external
        payable
        returns (uint256 amount0, uint256 amount1);

    function ownerOf(uint256 tokenId) external view returns (address);
    
    function tokenOfOwnerByIndex(address owner, uint256 index)
        external
        view
        returns (uint256);
}

// File: BaseModule.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "IGnosisSafe.sol";

contract BaseModule {
    ////////////////////////////////////////////////////////////////////////////
    // ERRORS
    ////////////////////////////////////////////////////////////////////////////

    error ExecutionFailure(address to, bytes data, uint256 timestamp);

    ////////////////////////////////////////////////////////////////////////////
    // INTERNAL
    ////////////////////////////////////////////////////////////////////////////

    /// @notice Allows executing specific calldata into an address thru a gnosis-safe, which have enable this contract as module.
    /// @param to Contract address where we will execute the calldata.
    /// @param data Calldata to be executed within the boundaries of the `allowedFunctions`.
    function _checkTransactionAndExecute(IGnosisSafe safe, address to, bytes memory data) internal {
        if (data.length >= 4) {
            bool success = safe.execTransactionFromModule(to, 0, data, IGnosisSafe.Operation.Call);
            if (!success) revert ExecutionFailure(to, data, block.timestamp);
        }
    }

    /// @notice Allows executing specific calldata into an address thru a gnosis-safe, which have enable this contract as module.
    /// @param to Contract address where we will execute the calldata.
    /// @param data Calldata to be executed within the boundaries of the `allowedFunctions`.
    /// @return bytes data containing the return data from the method in `to` with the payload `data`
    function _checkTransactionAndExecuteReturningData(IGnosisSafe safe, address to, bytes memory data)
        internal
        returns (bytes memory)
    {
        if (data.length >= 4) {
            (bool success, bytes memory returnData) =
                safe.execTransactionFromModuleReturnData(to, 0, data, IGnosisSafe.Operation.Call);
            if (!success) revert ExecutionFailure(to, data, block.timestamp);
            return returnData;
        }
    }
}


// File: IGnosisSafe.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

interface IGnosisSafe {
    event DisabledModule(address module);
    event EnabledModule(address module);

    enum Operation {
        Call,
        DelegateCall
    }

    /// @dev Allows a Module to execute a Safe transaction without any further confirmations.
    /// @param to Destination address of module transaction.
    /// @param value Ether value of module transaction.
    /// @param data Data payload of module transaction.
    /// @param operation Operation type of module transaction.
    function execTransactionFromModule(
        address to,
        uint256 value,
        bytes calldata data,
        Operation operation
    ) external returns (bool success);

    /// @dev Allows a Module to execute a Safe transaction without any further confirmations and return data
    /// @param to Destination address of module transaction.
    /// @param value Ether value of module transaction.
    /// @param data Data payload of module transaction.
    /// @param operation Operation type of module transaction.
    function execTransactionFromModuleReturnData(address to, uint256 value, bytes calldata data, Operation operation)
        external
        returns (bool success, bytes memory returnData);

    function enableModule(address module) external;
    
    function disableModule(address prevModule, address module) external;

    function getModules() external view returns (address[] memory);

    function isModuleEnabled(address module) external view returns (bool);
}

// File: UniswapV3HarvesterModuleConstants.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "INonfungiblePositionManager.sol";
import "IGnosisSafe.sol";

abstract contract UniswapV3HarvesterModuleConstants {
    address public constant GOVERNANCE = 0xD0A7A8B98957b9CD3cFB9c0425AbE44551158e9e;
    IGnosisSafe public constant SAFE = IGnosisSafe(GOVERNANCE);

    // Chainlink
    address constant CHAINLINK_KEEPER_REGISTRY = 0x02777053d6764996e594c3E88AF1D58D5363a2e6;

    // Uniswap V3
    INonfungiblePositionManager UNIV3_POSITION_MANAGER =
        INonfungiblePositionManager(0xC36442b4a4522E871399CD717aBDD847Ab11FE88);

    // readability constant
    uint128 constant UINT128_MAX = type(uint128).max;
}
//0x6352211e0000000000000000000000000000000000000000000000000000000000028c86

