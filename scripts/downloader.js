var csv = require('csvtojson');
var path = require('path');
var fs = require('fs');
var fetch = require('node-fetch');
var downloadedContracts = require('./downloadedContracts');

require('dotenv').config();

const projectPath = process.cwd();
const downloadedContractsPath = path.resolve(projectPath, 'scripts/downloadedContracts.json');
const codeLibsPath = path.resolve(projectPath, 'libs');
const contractaddressPath = path.resolve(process.cwd(), 'scripts/export-verified-contractaddress-opensource-license.csv');

const getUndownloadedContracts = async () => {
    const jsonArray = await csv().fromFile(contractaddressPath);
    const contractNameAddressMap = {};
    for (const contractInfo of jsonArray) {
        if (downloadedContracts.includes(contractInfo.ContractAddress)) {
            continue;
        }
        if (contractInfo.ContractName in contractNameAddressMap) {
            contractNameAddressMap[contractInfo.ContractName].push(contractInfo.ContractAddress);
        } else {
            contractNameAddressMap[contractInfo.ContractName] = [contractInfo.ContractAddress];
        }
    }
    return contractNameAddressMap;
}

function sleep(time = 0) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(true);
        }, time);
    });
}

const downloader = async (contractName, contractAddr) => {
    console.log('download: ', 'contractName:', contractName, 'contractAddr:', contractAddr);
    const resp = await fetch(`${process.env.API_URL}?module=contract&action=getsourcecode&address=${contractAddr}&apikey=${process.env.API_KEY}`).catch(e => {
        console.error("request api error", e)
        return undefined;
    })
    if (!resp) {
        return;
    }
    const r = await resp.json()
    if (r.message === 'OK' && r.result[0]?.SourceCode?.length > 0) {
        const code = r.result[0]

        let codeStr;
        let sourceCode = code.SourceCode.trimLeft().trimRight();

        if (sourceCode.startsWith("{{") && sourceCode.endsWith("}}")) {
            sourceCode = sourceCode.substring(1, sourceCode.length - 1)
            const obj = JSON.parse(sourceCode);
            if (obj?.sources) {
                let code = "";
                const files = obj?.sources;
                for (let key in files) {
                    code = `${code}\n// File: ${key}\n${files[key]["content"]}\n`;
                }
                codeStr = code;
            }
        } else {
            codeStr = sourceCode;
        }
        downloadedContracts.push(contractAddr);
        fs.writeFileSync(downloadedContractsPath, JSON.stringify(downloadedContracts));
        const solPath = path.resolve(codeLibsPath, `${contractName}/${contractAddr}.sol`);
        fs.writeFileSync(solPath, codeStr);
    }

    await sleep(1000);
}


const run = async () => {
    const undownloadedContracts = await getUndownloadedContracts();
    for (const contractName in undownloadedContracts) {
        const _projectPath = path.resolve(codeLibsPath, contractName);
        if (!fs.existsSync(_projectPath)) {
            fs.mkdirSync(_projectPath)
        }

        for (let contractAddr of undownloadedContracts[contractName]) {
            await downloader(contractName, contractAddr)
        }
    }
}

run()